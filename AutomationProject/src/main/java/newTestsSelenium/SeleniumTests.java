package newTestsSelenium;


import java.util.concurrent.TimeUnit;
import org.testng.annotations.*;


import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;


public class SeleniumTests {
	private WebDriver driver;
	private String baseUrl;
	

	/*
	 * @Test public void helpWikipedia() throws Exception {
	 * 
	 * System.setProperty("webdriver.gecko.driver",
	 * "C:\\geckodriver\\geckodriver.exe"); driver = new FirefoxDriver(); baseUrl =
	 * "https://en.wikipedia.org/"; driver.manage().timeouts().implicitlyWait(30,
	 * TimeUnit.SECONDS);
	 * 
	 * driver.get(baseUrl); driver.findElement(By.linkText("Help")).click();
	 * driver.findElement(By.name("search")).click();
	 * driver.findElement(By.name("search")).clear();
	 * driver.findElement(By.name("search")).sendKeys("help");
	 * driver.findElement(By.name("fulltext")).click();
	 * driver.findElement(By.id("mw-search-ns8")).click();
	 * driver.findElement(By.id("mw-search-ns11")).click();
	 * driver.findElement(By.cssSelector(
	 * "button.oo-ui-inputWidget-input.oo-ui-buttonElement-button")).click();
	 * driver.findElement(By.id("ooui-1")).clear();
	 * driver.findElement(By.id("ooui-1")).sendKeys("Islands of Adventure");
	 * driver.findElement(By.xpath(
	 * ".//*[@id='mw-search-top-table']/div[1]/div/div/span/span/button")).click();
	 * driver.close();
	 * 
	 * }
	 

	@Test
	public void seleniumTest1() {
		System.setProperty("webdriver.gecko.driver", "C:\\geckodriver\\geckodriver.exe");
		driver = new FirefoxDriver();
		baseUrl = "https://en.wikipedia.org/";
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		driver.get(baseUrl);
		WebElement arts = driver.findElement(By.xpath(".//*[@id='mp-topbanner']/ul/li[1]/a"));
		WebElement history = driver.findElement(By.xpath(".//*[@id='mp-topbanner']/ul/li[4]/a"));
		WebElement science = driver.findElement(By.xpath(".//*[@id='mp-topbanner']/ul/li[6]/a"));
		WebElement arts2 = driver.findElement(By.xpath(".//*[@id='portals-browsebar']/dl/dd[3]/a"));

		// Add all the actions into the Actions builder.
		Actions builder = new Actions(driver);
		builder.keyDown(Keys.CONTROL).click(arts).click(arts2).click(history).click(science).keyUp(Keys.CONTROL);
		// Perform the action.
		builder.perform();
	}*/
	@Test
	public void mouse()  {
		
		System.setProperty("webdriver.gecko.driver", "C:\\geckodriver\\geckodriver.exe");
		driver = new FirefoxDriver();
		baseUrl = "https://en.wikipedia.org/";
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		driver.get(baseUrl);
		WebElement arts = driver.findElement(By.xpath(".//*[@id='mp-topbanner']/ul/li[1]/a"));
		WebElement history = driver.findElement(By.xpath(".//*[@id='mp-topbanner']/ul/li[4]/a"));
		System.out.println("Arts location --> X coordinate: "+arts.getLocation().getX()+"  Y coordinate: "+arts.getLocation().getY());
		System.out.println("History location --> X coordinate: "+history.getLocation().getX()+"  Y coordinate: "+history.getLocation().getY());
		Actions builder = new Actions(driver);
		builder.moveByOffset(arts.getLocation().getX()+1, arts.getLocation().getY()+1).click();
		
		
		builder.perform();
		
		//driver.close();
		}
}